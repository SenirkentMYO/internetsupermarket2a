﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Supermarket
{
    public class Kullanici
    {
        public int KullaniciID;
        public string KullaniciAdi;
        public string KullaniciSoyadi;
        public string KullaniciSifre;
        public string KullaniciDogumYeri;
        public DateTime KullaniciDogumTarihi;
        public string KullaniciBolum;
        public DateTime KullaniciisBaslangicTarihi;
        public int KullaniciYetki;
        
    }
}