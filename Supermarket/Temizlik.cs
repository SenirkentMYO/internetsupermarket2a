﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Supermarket
{
    public class Temizlik
    {
        public int ID;
        public string Adi;
        public int Tur;
        public DateTime UretimTarihi;
        public DateTime SonTuketimTarihi;
        public string Marka;
        public float Fiyati;
    }
}