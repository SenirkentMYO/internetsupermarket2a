﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Supermarket
{
    public partial class Urungiris : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["GirenUye"] == null)
            {
                Response.Redirect("Default.aspx");
            }

            else
            {
                txturunid.Focus();
                lbluye.Text = "Hoşgeldin: " + ((Kullanici)Session["GirenUye"]).KullaniciAdi;
            }

        }
        protected void btUrunler_Click(object sender, EventArgs e)
        {
            Response.Redirect("AnaSayfa.aspx");
        }
        protected void btnurngrs_Click(object sender, EventArgs e)
        {
            Response.Redirect("Urungiris.aspx");
        }

        protected void btnkydt_Click(object sender, EventArgs e)
        {
            List<Urunler> liste = new List<Urunler>();
            SqlConnection giris = new SqlConnection(ConfigurationManager.ConnectionStrings["giris"].ToString());
            giris.Open();
            SqlCommand komut = new SqlCommand();
            komut.Connection = giris;
            komut.CommandText = "urunekleSp";
            komut.CommandType = CommandType.StoredProcedure;
            komut.Parameters.AddWithValue("@UrunID", txturunid.Text);
            komut.Parameters.AddWithValue("@UrunAdi", txturunadi.Text);
            komut.Parameters.AddWithValue("@UrunMarka", txturunmarkasi.Text);
            komut.Parameters.AddWithValue("@Fiyati", txturunfiyati.Text);
            komut.Parameters.AddWithValue("@sonKullanmaTarihi", DateTime.Parse(txtsonkullanma.Text));
            komut.ExecuteNonQuery();
            giris.Close();
            
        }

        protected void btnkullaniciekle_Click(object sender, EventArgs e)
        {
            Response.Redirect("KullaniciEkle.aspx");
        }
    }
}