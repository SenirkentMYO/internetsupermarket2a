﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Supermarket
{
    public partial class KullaniciEkle : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["GirenUye"] == null)
            {
                Response.Redirect("Default.aspx");
            }

            else
            {
                lbluye.Text = "Hoşgeldin: " + ((Kullanici)Session["GirenUye"]).KullaniciAdi;
            }
        }

        protected void btnkullaniciekle_Click(object sender, EventArgs e)
        {
            Response.Redirect("KullaniciEkle.aspx");
        }

        protected void btUrunler_Click(object sender, EventArgs e)
        {
            Response.Redirect("AnaSayfa.aspx");
        }

        protected void btnurngrs_Click(object sender, EventArgs e)
        {
            Response.Redirect("Urungiris.aspx");
        }

        protected void btnkydt_Click(object sender, EventArgs e)
        {
            List<Kullanici> liste = new List<Kullanici>();
            SqlConnection giris = new SqlConnection(ConfigurationManager.ConnectionStrings["giris"].ToString());
            giris.Open();
            SqlCommand komut = new SqlCommand();
            komut.Connection = giris;
            komut.CommandText = "kulekleSp";
            komut.CommandType = CommandType.StoredProcedure;
            komut.Parameters.AddWithValue("@kulID", txtkulid.Text);
            komut.Parameters.AddWithValue("@kultcno", txtkultc.Text);
            komut.Parameters.AddWithValue("@kulAdi", txtkuladi.Text);
            komut.Parameters.AddWithValue("@kulSoyad", txtkulsoyad.Text);
            komut.Parameters.AddWithValue("@kulSifre", txtkulsifre.Text);
            komut.Parameters.AddWithValue("@kuldogumyer", txtkuldgmyer.Text);
            komut.Parameters.AddWithValue("@kuldogumtar", DateTime.Parse(txtkuldgmtar.Text));
            komut.Parameters.AddWithValue("@kulbolum", txtkulbolum.Text);
            komut.Parameters.AddWithValue("@kulisbaslangic", DateTime.Parse(txtkulisbas.Text));
            komut.Parameters.AddWithValue("@kulyetki", txtkulyetki.Text);
            komut.ExecuteNonQuery();
            giris.Close();
        }
    }
}