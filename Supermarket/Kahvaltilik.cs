﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Supermarket
{
    public class Kahvaltilik
    {
        public int ID;
        public string Adi;
        public int KGAdet;
        public DateTime UretimTarihi;
        public DateTime SonTuketimTarihi;
        public string Marka;
        public float Fiyati;
        public int Adet;
    }
    }
