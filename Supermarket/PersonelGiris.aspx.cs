﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Supermarket
{
    public partial class PersonelGiris : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btUrunler_Click(object sender, EventArgs e)
        {
            Response.Redirect("AnaSayfa.aspx");
        }

        protected void btnpersonelgir_Click(object sender, EventArgs e)
        {
            Response.Redirect("PersonelGiris.aspx");
        }

        protected void btnurngrs_Click(object sender, EventArgs e)
        {
            Response.Redirect("Urungiris.aspx");
        }

        protected void btnkullaniciekle_Click(object sender, EventArgs e)
        {
            Response.Redirect("KullaniciEkle.aspx");
        }

        protected void btnkydt_Click(object sender, EventArgs e)
        {
            List<Personel> liste = new List<Personel>();
            SqlConnection giris = new SqlConnection(ConfigurationManager.ConnectionStrings["giris"].ToString());
            giris.Open();
            SqlCommand komut = new SqlCommand();
            komut.Connection = giris;
            komut.CommandText = "personelSp";
            komut.CommandType = CommandType.StoredProcedure;
            komut.Parameters.AddWithValue("@PerID", txtprsid.Text);
            komut.Parameters.AddWithValue("@PerAd", txtprsadi.Text);
            komut.Parameters.AddWithValue("@PerSoyad", txtprssoyad.Text);
            komut.Parameters.AddWithValue("@PerMaas", txtprsmaas.Text);
            komut.Parameters.AddWithValue("@PerAdres", txtprsadres.Text);
            komut.Parameters.AddWithValue("@PerTel", txtprstel.Text);
            komut.Parameters.AddWithValue("@Perisbas", DateTime.Parse(txtprsisbas.Text));
            komut.Parameters.AddWithValue("@PerDogumTar", DateTime.Parse(txtprsdgmtar.Text));
            komut.Parameters.AddWithValue("@PerDogumYer", txtprsdgmyer.Text);
            komut.Parameters.AddWithValue("@PerBolum", txtprsbol.Text);
            komut.ExecuteNonQuery();
            giris.Close();
        }
    }
}