﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Supermarket
{
    public class Personel
    {
        public int PersonelID;
        public string PersonelAdi;
        public string PersonelSoyad;
        public int PersonelMaas;
        public string PersonelAdres;
        public float PersonelTel;
        public DateTime Personelisbaslangic;
        public DateTime PersonelDogumTar;
        public string PersonelDogumYer;
        public string PersonelBolum;

    }
}