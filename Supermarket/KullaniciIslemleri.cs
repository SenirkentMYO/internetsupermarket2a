﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Supermarket
{
    public class KullaniciIslemleri
    {
        public Kullanici Giris(string kulAd, string kulSifre)
        {
            List<Kullanici> listeKullanici = new List<Kullanici>();
            SqlConnection giris = new SqlConnection(ConfigurationManager.ConnectionStrings["giris"].ToString());
            giris.Open();
            SqlCommand komut = new SqlCommand();
            komut.Connection = giris;
            komut.CommandType = CommandType.StoredProcedure;
            komut.CommandText = "GirisSp";
            SqlParameter prmtre1 = new SqlParameter();
            prmtre1.DbType = DbType.String;
            prmtre1.ParameterName = "@GelenAd";
            prmtre1.Value = kulAd;
            SqlParameter prmtre2 = new SqlParameter();
            prmtre2.DbType = DbType.String;
            prmtre2.ParameterName = "@GelenSifre";
            prmtre2.Value = kulSifre;
            komut.Parameters.Add(prmtre1);
            komut.Parameters.Add(prmtre2);
            SqlDataReader rows = komut.ExecuteReader();
            while (rows.Read())
            {
                
            }
            
            if (listeKullanici.Count > 0)
            {
                return listeKullanici[0];
            }
            else
            {
                return null;
            }
        }
    }
}