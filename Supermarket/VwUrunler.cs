﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Supermarket
{
    public class VwUrunler
    {

        public int UrunID
        {
            get;
            set;
        }
        public string UrunAdi
        {
            get;
            set;
        }
        public string UrunMarkasi
        {
            get;
            set;
        }
        public float Fiyati
        {
            get;
            set;
        }
        public DateTime sonKullanmaTarihi
        {
            get;
            set;

        }
    }
}