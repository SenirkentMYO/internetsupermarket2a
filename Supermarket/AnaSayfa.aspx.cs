﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

namespace Supermarket
{
    public partial class AnaSayfa : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["GirenUye"] == null)
            {
                Response.Redirect("Default.aspx");
            }

            else
            {
                lbluye.Text = "Hoşgeldin: " + ((Kullanici)Session["GirenUye"]).KullaniciAdi;
            }
            
        }

        protected void btUrunler_Click(object sender, EventArgs e)
        {
            List<VwUrunler> listUrunler = new List<VwUrunler>();
            using (SqlConnection giris = new SqlConnection(ConfigurationManager.ConnectionStrings["giris"].ToString()))
            {
                giris.Open();
                string sorguUrunler = "Select * From Urunler";
                SqlCommand komutUrunler = new SqlCommand(sorguUrunler, giris);
                SqlDataReader verial = komutUrunler.ExecuteReader();
                while (verial.Read())
                {
                    VwUrunler li = new VwUrunler();
                    li.UrunID = (int)verial["UrunID"];
                    li.UrunAdi = (string)verial["urunAdi"];
                    li.sonKullanmaTarihi = (DateTime)verial["sonKullanmaTarihi"];
                    li.Fiyati = (float)verial["Fiyati"];
                    li.UrunMarkasi = (string)verial["UrunMarkasi"];
                    
                    listUrunler.Add(li);
                }
                giris.Close();
                giris.Dispose();
            }
            Repeater1.DataSource = listUrunler;
            Repeater1.DataBind();

        }
        protected void btnurngrs_Click(object sender, EventArgs e)
        {
            Response.Redirect("Urungiris.aspx");
        }

        protected void btnKullanici_Click(object sender, EventArgs e)
        {
            List<Kullanici> listKullanici=new List<Kullanici>();
            using (SqlConnection giris = new SqlConnection(ConfigurationManager.ConnectionStrings["giris"].ToString()))
            {
                giris.Open();
                string sorguKullanici = "SELECT * FROM Kullanici";
                SqlCommand komutKullanici = new SqlCommand(sorguKullanici,giris);
                SqlDataReader kullaniciAl = komutKullanici.ExecuteReader();
                while (kullaniciAl.Read())
                {
                    Kullanici kullanici = new Kullanici();
                    kullanici.KullaniciID = (int)kullaniciAl["KullaniciID"];
                    kullanici.KullaniciAdi = kullaniciAl["KullaniciAdi"].ToString();
                    kullanici.KullaniciSoyadi = kullaniciAl["KullaniciSoyadi"].ToString();
                    kullanici.KullaniciSifre = (string)kullaniciAl["KullaniciSifre"];
                    kullanici.KullaniciDogumYeri = kullaniciAl["KullaniciDogumYeri"].ToString();
                    kullanici.KullaniciDogumTarihi = (DateTime)kullaniciAl["KullaniciDogumTarihi"];
                    kullanici.KullaniciBolum = kullaniciAl["KullaniciBolum"].ToString();
                    kullanici.KullaniciisBaslangicTarihi = (DateTime)kullaniciAl["KullaniciisBaslangicTarihi"];
                    
                    listKullanici.Add(kullanici);

                }
                giris.Close();
                giris.Dispose();
            }
            Repeater1.DataSource = listKullanici;
            Repeater1.DataBind();
        }

        protected void btnkullaniciekle_Click(object sender, EventArgs e)
        {
            Response.Redirect("KullaniciEkle.aspx");
        }

        protected void btnUrunSil_Click(object sender, EventArgs e)
        {
            //Response.Redirect("UrunSil.aspx");
            List<VwUrunler> urunList = new List<VwUrunler>();
            using (SqlConnection urunler = new SqlConnection(ConfigurationManager.ConnectionStrings["urunler"].ToString()))
            {
                urunler.Open();
                string sorguUrunler = "Select * From Urunler";
                SqlCommand komutUrunler = new SqlCommand(sorguUrunler, urunler);
                SqlDataReader urunal = komutUrunler.ExecuteReader();
                while (urunal.Read())
                {
                    VwUrunler liste = new VwUrunler();
                    liste.UrunID = (int)urunal["UrunID"];
                    liste.UrunAdi = (string)urunal["urunAdi"];
                    urunList.Add(liste);
                }
            }
            Repeater1.DataSource = urunList;
            Repeater1.DataBind();
            using (SqlConnection sil = new SqlConnection(ConfigurationManager.ConnectionStrings["sil"].ToString()))
            {
                sil.Open();
                SqlCommand komutsil = new SqlCommand("DELETE * FROM VwUrunler Where (Repeater1.SelectedRows[0])", sil);
                SqlDataReader urunsil = komutsil.ExecuteReader();
                komutsil.ExecuteNonQuery();
            }
        }
        
    }
}