﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Supermarket
{
    public partial class PersonelListe : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

       

        protected void btnPerList_Click(object sender, EventArgs e)
        {
            List<Personel> listPersonel = new List<Personel>();
            using (SqlConnection giris = new SqlConnection(ConfigurationManager.ConnectionStrings["giris"].ToString()))
            {
                giris.Open();
                string sorguPersonel = "Select * From Personel";
                SqlCommand komutPersonel = new SqlCommand(sorguPersonel, giris);
                SqlDataReader verial = komutPersonel.ExecuteReader();
                while (verial.Read())
                {
                    Personel pe = new Personel();
                    pe.PersonelID = (int)verial["PersonelID"];
                    pe.PersonelAdi = (string)verial["PersonelAdi"];
                    pe.PersonelSoyad = (string)verial["PersonelSoyad"];
                    pe.PersonelMaas = (int)verial["PersonelMaas"];
                    pe.PersonelAdres = (string)verial["PersonelAdres"];
                    pe.PersonelTel = (float)verial["PersonelTel"];
                    pe.Personelisbaslangic = (DateTime)verial["Personelisbaslangic"];
                    pe.PersonelDogumTar = (DateTime)verial["PersonelDogumTar"];
                    pe.PersonelDogumYer = (string)verial["PersonelDogumYer"];
                    pe.PersonelBolum = (string)verial["PersonelBolum"];

                    listPersonel.Add(pe);
                }
                giris.Close();
                giris.Dispose();
            }
            Repeater2.DataSource = listPersonel;
            Repeater2.DataBind();
        }

        protected void btUrunler_Click(object sender, EventArgs e)
        {
            Response.Redirect("AnaSayfa.aspx");
        }

        protected void btnurngrs_Click(object sender, EventArgs e)
        {
            Response.Redirect("Urungiris.aspx");
        }

        protected void btnkullaniciekle_Click(object sender, EventArgs e)
        {
            Response.Redirect("KullaniciEkle");
        }

        protected void btnUrunSil_Click(object sender, EventArgs e)
        {

        }
    }
}